FROM r-base
COPY . /usr/local/src/ltv_progect
WORKDIR /usr/local/src/ltv_progect
RUN R -e "install.packages('DBI',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('bigrquery',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('data.table',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('dplyr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('glue',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('futile.logger',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('stringi',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('lubridate',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('colf',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('countrycode',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('gargle',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('jsonlite',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('httr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
CMD ["Rscript", "global.R"]

