import pandas as pd
import datetime as dt
import numpy as np
import os
import requests
import tempfile
from google.cloud import bigquery
from google.cloud import bigquery_storage_v1
from functools import partialmethod

client = bigquery.Client.from_service_account_json('./creds.json')
bigquery_storage_v1.client.BigQueryReadClient.read_rows = partialmethod(bigquery_storage_v1.client.BigQueryReadClient.read_rows, timeout=3600*2) 
bq_storage_client = bigquery_storage_v1.BigQueryReadClient.from_service_account_json('./creds.json')
project_id = 'goingtoclouds'

sql = '''
with feed as (
   select picture_id, feed_type, tags 
    from `goingtoclouds.feed.all_feed` 
    where feed_type = 'base_feed'

    union all 
    select picture_id, feed_type, tags 
    from `goingtoclouds.feed.all_feed` 
    where feed_type = 'daily_feed' and tags is not null 
    
    union all 
    select distinct picture_id, feed_type, "daily" as  tags 
    from `goingtoclouds.feed.all_feed` 
    where feed_type = 'daily_feed' 

    union all 
    select picture_id, feed_type, tags 
    from `goingtoclouds.feed.all_feed` 
    where feed_type = 'bonus_feed' and tags is not null 

    union all 
    select distinct picture_id, feed_type, "bonus" as  tags 
    from `goingtoclouds.feed.all_feed` 
    where feed_type = 'bonus_feed'
)

select customer_user_id, total_finished_pics, tags, count(distinct picture_id) as tag_finished_pics
from (
    SELECT customer_user_id, a.picture_id, tags,
        count(distinct a.picture_id) over(partition by customer_user_id) as total_finished_pics
    FROM `goingtoclouds.aggregated_data.picture_finish` as a
    left join feed as b
    on a.picture_id=b.picture_id
    where tags is not null and customer_user_id is not null
    and country != 'Belarus' and customer_user_id != 31)
group by customer_user_id, total_finished_pics, tags
'''
fin_pics = client.query(sql, project=project_id).to_dataframe()

fin_pics = pd.pivot_table(fin_pics, values='tag_finished_pics', index=['customer_user_id', 'total_finished_pics'], 
                          columns=['tags']).reset_index()
fin_pics = fin_pics[fin_pics.customer_user_id != 0]
fin_pics.replace(np.nan, 0, inplace = True)

X = fin_pics.loc[:, fin_pics.columns != 'customer_user_id']
X