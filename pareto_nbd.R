packageLoad <- function() {
  
  packages <- c('DBI', 'bigrquery', 'data.table', 'dplyr', 'glue', 'futile.logger',
                'stringi', 'lubridate', 'colf', 'countrycode', 'gargle', 'jsonlite', 
                'httr', 'BTYD', 'RMySQL')
  for(package in packages) {
    library(package, character.only = TRUE, quietly = TRUE)
  }
}

packageLoad()


options(scipen = 20)
billing <- "goingtoclouds"

# load all data with transactions
testers_idfas <- c("B195BE64-3E18-46C1-8876-ACBF0AE9C4A1", "A0567C32-4973-4A23-B870-ACE32606D744",
                   "A93317D9-F99F-4B98-B840-93983AD9040C", "2D0D71CC-53CE-434B-BED7-3A741ECC1551",
                   "2D5342F6-3A36-4B84-A086-12CC1DED4032", "4A7C1434-D7DC-4721-B089-9710214A0398",
                   "745E057F-1E6B-48DC-94A6-A8982825A914", "D00D7242-79F3-4E41-81CA-8010A657DD71",
                   "D14E17D1-F627-4BD2-B6D1-D09BB7C62932", "6F938C35-D3D4-42F8-BA34-10EA5385CAE6",
                   "071198F6-C77F-4F3F-B4F3-8694EA72E0DE", "44DFF205-3089-4CCD-B21F-6C80C8F3395C",
                   "64004C31-55DD-4CE0-B156-E56D98381F47", "E72DAF58-217F-4A16-BAA5-9B29E1AD80B8",
                   "2D5342F6-3A36-4B84-A086-12CC1DED4032", "8BB8D049-7DEC-4C6F-9ECA-67B95FC41CA7",
                   "CF5D500E-89A7-4CE1-9487-85475C79A842", "870641D8-4FFB-4C7D-8F0F-28C0FA483F03",
                   "AD9AD4EA-70D1-47D0-9238-CFBCF461D2F8", "F8A43D3D-F889-4F99-A3A1-A0109C637092",
                   "A504E9FB-8445-4C3F-A99F-FE220D850D36", "83B65FD0-74AA-48F2-9D03-4B0E16626904",
                   "7B6E2BDB-8672-4FE2-9586-2E7C4A315F11", "2D275100-F92A-4352-AABA-514ECE03ADE4",
                   "271D47CD-2B95-40A7-A409-1EA968933FD0", "6B59854F-342D-403B-91DF-71D99A70CFF2",
                   "1FFCABB2-178D-44E0-B46D-2B3D93E1E5BC", "6166E8B4-CE92-468E-8940-0034C6006744",
                   "3B9EAC00-B3B2-42BE-9F4E-E64AEC76F615", "9CB511A7-EB3B-40F4-964A-58A461B4F26F",
                   "4DE54312-FE84-423E-85A1-07F0AE25C05D", "6C107872-3442-495E-8E13-8F17EB12C9E8",
                   "B8314920-A9EE-4499-955A-3BF13F750C23", "EA0BFC1C-2A83-4CC1-879C-9595B4CD74CB",
                   "1D4ACF2D-1D97-47F9-8FE5-8B0EC09D1132", "B704351D-5E0A-45CD-9A08-527DED373988",
                   "990F0F46-097A-4887-A369-D5B7972DE85A", "39265650-A77D-4F0D-88BA-488C23FC0F9D",
                   "137092C2-6CA8-4BC1-91BF-74FDF7EDE0AA", "EF0AD41F-111F-49CC-8AF7-83476889BDB3",
                   "4C579873-502C-4DEA-BDA9-73A7B13BD820", "9A77B739-1FD0-4022-9EFB-902D1CE00537",
                   "F275CBC4-78D5-454A-90CC-E13CDE312350", "B825A92C-B1D1-42E1-8E42-4E1F5AEE16B7",
                   "EA39F2CF-36B4-4725-AB57-928B56AD9A80", "1690318B-CF1E-417E-ABF5-DDE8DF98332E",
                   "E914F992-E7C6-4FF8-BA35-4C4BF5671805", "1BD8F3F6-51B4-436A-A826-A1B13CD1D061",
                   "8EB38163-323B-4F4D-9E1A-F52FCE8150E8", "7337D2B6-3FA2-42C0-A760-1FF865F3C666",
                   "23792B06-5B89-4E70-82F2-7B440F468E7C", "158843B2-78C3-49F5-BF45-C68C3B46F7E1",
                   "7BF5D98B-B0B4-4D53-9A4D-C7FAE5047C84", "ADC51C02-A716-41F7-B04C-71DA7E4EFE83",
                   "2BCB677D-26F5-479B-B167-11466C3ABDE1", "F817D57B-EE30-41BB-B55F-45BDFE9025AO",
                   "1DA9FD63-CC70-42F9-B113-5DD80E7CABCE", "7344F3D0-3B31-4231-8068-C1D29DA27B66")
testers_idfas <- stri_c(testers_idfas, collapse = "','")

# load all revenue 
sql <- "SELECT a.transaction_date, a.user_id, SUM(a.revenue/(1 - IFNULL(`hc_compendium.revenue_compendium`.coef, 0))) as calc_rev
FROM (
  SELECT date as transaction_date, user_id, platform, sum(revenue) as revenue 
  FROM `goingtoclouds.applovin_max.max_user_revenue`  
  GROUP BY date, user_id, platform
  UNION ALL
  SELECT date as transaction_date, user_id, 'ios' as platform, sum(revenue) as revenue 
  FROM `goingtoclouds.ironsource_happy_canvas.ironsource_revenue` 
  GROUP BY date, user_id, platform) AS a
LEFT JOIN `hc_compendium.revenue_compendium` 
ON a.platform = `hc_compendium.revenue_compendium`.platform AND a.transaction_date = `hc_compendium.revenue_compendium`.date
GROUP BY a.transaction_date, a.user_id"

transactions <- bq_project_query(billing, sql)
transactions_dt <- bq_table_download(transactions)
setDT(transactions_dt)


# connect to my sql for iaps extraction
conn <-  dbConnect(MySQL(), user = 'hc_admin', 
                   password = '>ReApp*?^<8_P)DJ',
                   dbname = 'hc_prod', 
                   host = 'hc-prod-rds2.cwydukect5ey.us-east-1.rds.amazonaws.com')

sql <- "select DATE(purchaseDate) as transaction_date, memberId as user_id, 9.99 as calc_rev
from hc_prod.InAppPurchase iap 
where cost is not NULL" 
rs <- dbSendQuery(conn, sql)
iaps <-  fetch(rs, n = -1)
setDT(iaps)
on.exit(dbDisconnect(conn))

iaps[, transaction_date := as.Date(transaction_date)]
transactions_dt <- rbind(iaps, transactions_dt)
setnames(transactions_dt, c('calc_rev', 'user_id', 'transaction_date'), c('sales', "cust", "date"))

transactions_dt <- transactions_dt[, .(sales = sum(sales)), by = .(cust, date)]
transactions_dt[, min_date := min(date), by = cust]

# remove users which appear after calibration period
users_after_sep <- transactions_dt[min_date > as.Date('2020-09-01'), unique(cust)]
transactions_dt <- transactions_dt[!cust %in% users_after_sep]

training_dt <- transactions_dt[date <= as.Date('2020-09-01') ]

cal_cbs_training <- training_dt[, .(x = .N-1, t.x = as.integer(max(date) - min(date)), 
                                    T.cal = as.integer(as.Date('2020-09-01') - min(date))),
                                by = cust] 


# bg/nbd model -----------------------------------------------------------------
params <- bgnbd.EstimateParameters(cal_cbs_training)
LL <- bgnbd.cbs.LL(params, cal_cbs_training)

p.matrix <- c(params, LL)

for (i in 1:10) {
  print(i)
  params <- bgnbd.EstimateParameters(cal_cbs_training, params)
  LL <- bgnbd.cbs.LL(params, cal_cbs_training)
  p.matrix.row <- c(params, LL)
  p.matrix <- rbind(p.matrix, p.matrix.row)
}

colnames(p.matrix) <- c("r", "alpha", "a", "b", "LL")
rownames(p.matrix) <- 1:11
p.matrix
params <- p.matrix[11, 1:4]

bgnbd.PlotDropoutHeterogeneity(params)
bgnbd.PlotDropoutRateHeterogeneity(params)
bgnbd.Expectation(params, t = 180)
  

cal_cbs_training[cust == 159622,]
# x t.x T.cal
# 26.00 30.86 31.00
x <- cal_cbs_training[cust == 159622, x]
t.x <- cal_cbs_training[cust == 159622, t.x]
T.cal <- cal_cbs_training[cust == 159622, T.cal]

bgnbd.ConditionalExpectedTransactions(params, T.star = 60, x, t.x, T.cal)
bgnbd.PlotFrequencyInCalibration(params, cal_cbs_training, 10)

# add number of transactions in holdout period 
cal_cbs_training <- merge(cal_cbs_training, transactions_dt[date > as.Date('2020-09-01'), .(x.star = .N), by = cust], 
                          by = 'cust', all.x = TRUE)
cal_cbs_training[is.na(x.star), x.star := 0]

T.star <- 43 # length of the holdout period

comp <- bgnbd.PlotFreqVsConditionalExpectedFrequency(params, T.star, cal_cbs_training, cal_cbs_training$x.star, 15)
comp

cal_cbs_training[, p_alive := bgnbd.PAlive(params, x, t.x, T.cal), by = cust]
cal_cbs_training[, inactivity_days := T.cal - t.x]

cal_cbs_training$p_range <- cut(cal_cbs_training$p_alive, breaks = seq(0,1, by = 0.1), ordered_result = TRUE)

library(ggplot2)

ggplot(cal_cbs_training, aes(x = inactivity_days, y = p_alive)) + 
  geom_point()

# inc.tracking <- transactions_dt[date != min_date, .(n = .N), by = date][order(date)]$n
# daily_plot <- bgnbd.PlotTrackingInc(params, T.cal = cal_cbs_training$T.cal, T.tot = 195, inc.tracking,
#                       xlab = "Day", title = "Tracking Daily Transactions")
# 
# cum.tracking.data <- cumsum(inc.tracking)
# cum.tracking <- bgnbd.PlotTrackingCum(params, T.cal, T.tot, cum.tracking.data, n.periods.final)


# 

data(cdnowSummary)
cal.cbs <- cdnowSummary$cbs
# cal.cbs already has column names required by method

# Cumulative repeat transactions made by all customers across calibration
# and holdout periods
cu.tracking <- cdnowSummary$cu.tracking
# make the tracking data incremental
inc.tracking <- dc.CumulativeToIncremental(cu.tracking)

# parameters estimated using bgnbd.EstimateParameters
est.params <- c(0.243, 4.414, 0.793, 2.426)

# All parameters are in weeks; the calibration period lasted 39
# weeks and the holdout period another 39.
bgnbd.PlotTrackingInc(est.params, T.cal=cal.cbs[,"T.cal"], T.tot = 78, inc.tracking)

