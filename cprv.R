# Load and install necessary libraries -----------------------------------------
packageLoad <- function() {
  
  packages <- c('DBI', 'bigrquery', 'data.table', 'dplyr', 'glue', 'futile.logger',
                'stringi', 'lubridate', 'colf')
  packageCheck <- match(packages, utils::installed.packages()[,1])
  
  installPackages <- packages[is.na(packageCheck)]
  if(length(installPackages) > 0L) {
    utils::install.packages(installPackages,
                            repos = "https://cloud.r-project.org/"
    )
  } else {
    print('All requested packages have been already installed')
  }
  for(package in packages) {
    library(package, character.only = TRUE, quietly = TRUE)
  }
}

packageLoad()

# Extract preview page events --------------------------------------------------

options(scipen = 20)
billing <- "goingtoclouds" # replace this with your project ID 

daterange <- seq.Date(as.Date('2020-06-13'), as.Date('2020-09-16'), by = 'day')

daterange <- stri_replace_all_fixed(as.character(daterange), '-', '')

all_dt <- data.table()

for (i in daterange) {
  
  print(i)
  
  sql <- glue("SELECT event_name, event_date, param.value.string_value as custom_user_id, count(*) as n
FROM `lwapps-coloring.analytics_218793363.events_{i}`,
unnest(user_properties) as param 
WHERE  event_name IN ('bm_added', 'prelaunch_view', 'preview_color_rv', 'prelaunch_close', 'picture_start') 
AND param.key = 'user_id'
group by event_name, event_date, custom_user_id")
  
  events <- bq_project_query(billing, sql)
  dt <- bq_table_download(events)
  setDT(dt)
  
  all_dt <- rbind(all_dt, dt)
  
}


# dt <- copy(all_dt)
all_dt[, event_date := as.Date(event_date, tryFormats = "%Y%m%d")]
all_dt[, .(sum(n)), by = event_name]


# count all events 
all_events <- all_dt[, .(n = sum(n)), by = .(event_name, event_date)][order(event_date)]
all_events <- merge(all_events, all_events[event_name == 'prelaunch_view', .(prelaunch_view = n), by = event_date], by = 'event_date', all.x = TRUE)

all_events[event_name %in% c("prelaunch_close", "preview_color_rv", "bm_added", "picture_start"), 
           share := n / prelaunch_view]

all_events <- all_events[!is.na(share) & event_date > as.Date('2020-06-14')]
all_events[, weekday := weekdays(event_date)]

# add other
all_events <- rbind(all_events, all_events[, .(event_name = 'other', n = prelaunch_view - sum(n), share = 1-sum(share)), by = .(event_date, prelaunch_view)])

# all_events[weekday == 'суббота', .(mean(share)), by = .(weekday, event_name)][order(event_name, weekday)]

write.csv(all_events, 'all_events_3.csv')


all_events[event_date <= as.Date('2020-08-20') & event_name == 'prelaunch_close', mean(share)]
all_events[event_date > as.Date('2020-08-20') & event_name == 'prelaunch_close', mean(share)]

all_events[, mean(share), by = event_name]

all_events[event_date <= as.Date('2020-08-20') & event_name == 'picture_start', mean(share)]
all_events[event_date > as.Date('2020-08-20') & event_name == 'picture_start', mean(share)]

all_events[event_date <= as.Date('2020-08-20') & event_name == 'bm_added', mean(share)]
all_events[event_date > as.Date('2020-08-20') & event_name == 'bm_added', mean(share)]

all_events[event_date <= as.Date('2020-08-20') & event_name == 'preview_color_rv', mean(share)]
all_events[event_date > as.Date('2020-08-20') & event_name == 'preview_color_rv', mean(share)]

# load first installs 
flog.info("Loading data with first installs...")

sql <- glue("SELECT b.customer_user_id, a.appsflyer_id, a.date AS first_cohort_date
  FROM (
    SELECT appsflyer_id, min(DATETIME(TIMESTAMP(event_time))) AS date
    FROM `goingtoclouds.appsflyer_happy_canvas.appsflyer_installs`
    group by appsflyer_id) AS a
  LEFT JOIN  (
    SELECT distinct appsflyer_id, customer_user_id 
    FROM `goingtoclouds.appsflyer_happy_canvas.appsflyer_registration` 
  ) AS b
  ON a.appsflyer_id = b.appsflyer_id
")

install <- bq_project_query(billing, sql)
install_dt <- bq_table_download(install)
setDT(install_dt)

install_dt[, customer_user_id := as.integer(customer_user_id)]
all_dt[, custom_user_id := as.integer(custom_user_id)]

install_dt <- install_dt[!is.na(customer_user_id)]


tb <- merge(all_dt[event_date > as.Date('2020-06-14')], install_dt, by.x = 'custom_user_id', by.y = 'customer_user_id', all.x = TRUE)

tb[, first_cohort_date := as.Date(first_cohort_date)]
tb[, visit_day := as.integer(event_date - first_cohort_date)]
tb <- tb[!custom_user_id %in% tb[visit_day < 0, unique(custom_user_id)]] 

tb <- tb[, .(clicks = sum(n)), by = .(visit_day, event_name)]

tb <- merge(tb, tb[event_name == 'prelaunch_view', .(prelaunch_view = clicks), by = visit_day], by = 'visit_day', all.x = TRUE)
tb <- tb[!is.na(visit_day), ]

tb[, share :=  clicks / prelaunch_view]

tb <- tb[event_name != 'prelaunch_view']

# add other 
tb <- rbind(tb, tb[, .(event_name = 'other', clicks = prelaunch_view - sum(clicks), share = 1-sum(share)), by = .(visit_day, prelaunch_view)])


tb[visit_day >10 & visit_day <= 100, mean(share), by = event_name]

# write.csv(tb, 'tb_1.csv')

# find other events ------------------------------------------------------------

sql <- glue("SELECT event_name, event_timestamp, param.value.string_value as custom_user_id
FROM `lwapps-coloring.analytics_218793363.events_20200915`,
unnest(user_properties) as param 
WHERE  event_name IN ('bm_added', 'prelaunch_view', 'preview_color_rv', 'prelaunch_close', 'picture_start') 
AND param.key = 'user_id'")

events <- bq_project_query(billing, sql)
dt <- bq_table_download(events, bigint =  "integer64")
setDT(dt)

dt[, timestamp := as.POSIXct(event_timestamp/1000000, tz = 'UTC', origin = '1970-01-01')]
setorder(dt, custom_user_id, timestamp)


dt[, id := seq_len(.N), by = custom_user_id]















